package by.EPAM.javatrain;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Locale;

/***
 * 
 * @author Sergii_Kotov
 *	Печать календаря
 */
public class CalendarTest {
	static int today;// класс, объект которого выводит на консоль календарь на год - это самостоятельная сущность
	// вы переувлеклись статикой, при том забыли даже об атрибутах доступа
	static int month;
	/***
	 * пока печатает все 12 мес. или кол-во месяцев кратное  mInLineCount
	 */
	static final int startMonth=0;
	/***
	 * количество месяцев начиная со startMonth
	 */
	static final int mCount=12;
	/***
	 * количество месяцев в строке.
	 * Желательно 3, 4 или 6
	 */
	static final int mInLineCount=2;

	public static void main(String[] args)	
	{	
		//запомним текущий день чтобы отметить его звездочкой
		GregorianCalendar d = new GregorianCalendar();
		today = d.get(Calendar.DAY_OF_MONTH);
		month = d.get(Calendar.MONTH);
		
		// массивы названий и номеров месяцов
		String [] sMonth = new String [mCount];
		int [] iMonth = new int[mCount];
		// заполняем массив ссылок на месяца
		GregorianCalendar [] arG= new GregorianCalendar [mCount];
		for (int i=0; i<mCount; i++) {
			arG[i]=new GregorianCalendar();
			// каждый след. месяц сдвигаем на 1 
			arG[i].set(Calendar.MONTH, i+startMonth);
			// для каждого месяца устанавливаем дату начала в 1е число . 
			arG[i].set(Calendar.DAY_OF_MONTH, 1);
			// названия и номера месяцев запомним такими какие были при создании
			sMonth[i] = arG[i].getDisplayName( Calendar.MONTH, Calendar.LONG, Locale.getDefault());
			iMonth[i]=i+startMonth;
		}	
		// основной цикл по линиям в которых располагаются месяцы
		for (int ln=1; ln<=((mCount-startMonth)/mInLineCount); ln++) {
			// для каждой линии шапка из названий месяцов и дней недели
			for (int j=mInLineCount*(ln-1); j<mInLineCount*ln; j++) {
				System.out.format("%-24s", sMonth[j]);
			}
			System.out.println();
			for (int j=mInLineCount*(ln-1); j<mInLineCount*ln; j++) {
				System.out.print("Вс Пн Вт Ср Чт Пт Сб    ");
			}
			System.out.println();
			//вывод чисел. На каждый месяц может понадобится макс. 6 строк.
			for (int i=0; i<6; i++) {
				for (int j=mInLineCount*(ln-1); j<mInLineCount*ln; j++){
					// если только начало месяца - добьем его пробелами
					if (i==0){
						printInitialWeek(arG[j]);
					}	
					printWeek(arG[j],iMonth[j], ((j+1)%mInLineCount)==0);
				}
			}   
		}

	}
	/***
	 * вывод пробелов вместо дней предыдущего месяца в начале текущего (того который в данный момент выводим) месяца
	 * @param g ссылка на календарь текущего месяца
	 */
	private static void printInitialWeek (GregorianCalendar g) {
		for (int i = Calendar.SUNDAY; i < g.get(Calendar.DAY_OF_WEEK); i++)
			System.out.print("   ");
	}
	
	/***
	 * Печать недели. 
	 * Если неделя первая, то допечатываем после пробелов
	 * Если неделя в месяце, который крайний в строке, то после нее переносим строку
	 * Если неделя содержит числа следующего месяца, выводим пробелы вместо них 
	 * @param g ссылка на календарь текущего месяца
	 * @param monthCurr текущий месяц, который запомнили при инициализации календарей
	 * @param lastInLine это крайний месяц в строке?
	 */
	private static void printWeek(GregorianCalendar g, int monthCurr, boolean lastInLine) {
	    int weekday;
		do {
			int day = g.get(Calendar.DAY_OF_MONTH);
			if (monthCurr != g.get(Calendar.MONTH)) {
				//числа следующего месяца
				System.out.print("   ");
			} else {  
				System.out.print(day);
				if (day < 10)
					System.out.print(" ");
				if ((day == today)&&( monthCurr== month))
					// запомненное до заполнения каледаря текущее число
					System.out.print("*");
				else
					System.out.print(" ");
			}
			g.add(Calendar.DAY_OF_MONTH, 1);
			weekday = g.get(Calendar.DAY_OF_WEEK);
		} while (weekday != Calendar.SUNDAY);
		if (lastInLine) {
			System.out.println();
		} else {
			System.out.print("   ");
		}
	}
}	