package by.EPAM.javatrain;

import java.util.Comparator;
import java.util.Collections;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Arrays;

/***
 * Создать класс “Записная книжка”. 
 * Предусмотреть возможность работы с произвольным числом записей, поиска записи по какому-либо признаку 
 * (например, по фамилии, дате рождения или номеру телефона), 
 * добавления и удаления записей, сортировки по разным полям.
 * @author Sergii_Kotov
 * некошерно конечно держать тут работу с консолью, но это же учебный пример, да?
 *
 */
public class NoteBook {
	/***
	 * список записей
	 */
 	ArrayList<Record> noteBook = new ArrayList<Record>();// куда исчезли атрибуты доступа?
 	
 	/***
 	 * конструктор по умолчанию заполняет тестовыми данными
 	 */
 	public NoteBook() {
 		randomFill();
 	}
 	
 	public void randomFill() {
 		for (int i=0; i<10; i++) {
			noteBook.add(new Record("s"+i, "n"+Math.round(Math.random()*10), "sc"+i, i+"_132132", i+"dfasdf132", new Date()));
			//noteBook.get(i).show();
		}
 	}

 	/***
 	 * удаление записи из списка записной книжки
 	 * @param jR ссылка на удаляемую запись, которая если будет найдена - удалится из записной книжки 
 	 */
 	public void delRecord(Record jR) {
		if (noteBook.contains(jR)) {
			noteBook.remove(jR);
			System.out.println("удален: "+jR);
		} 
 	}
 	
 	/***
 	 * Добавление с проверкой на существование. чтобы не перезаписать запись с 
 	 * такими же телефоном и фамилией
 	 * @param jR добавляемая запись
 	 */
 	public void addRecord(Record jR) {
 		//Record jR = new Record("s3", "n", "sc", "3_132132", "dfasdf132", null);
		if (!noteBook.contains(jR)) {
			noteBook.add(jR);
			System.out.println("добавлен");
		} else {
			System.out.println("Такая запись уже существует!");
			// покажем существующую и добавляемую записи
			for (Record h :noteBook) {
				if (h.equals(jR)){
					h.show();		
					jR.show();
				}
			}
		} 		
 	}
 	
 	/***
 	 * сортировка по имени. Элементы списка записной книжки сортируются 
 	 * по возрастанию имени
 	 */
 	public void sortByName() {
 		Collections.sort(noteBook, new Comparator<Record>() {
	        @Override
	        public int compare(Record r1, Record r2)
	        {
	            return  r1.name.compareTo(r2.name);
	        }
	    });
 	}

 	/***
 	 * сортировка по имени. Элементы списка записной книжки сортируются 
 	 * по возрастанию телефона
 	 */
 	public void sortByPhone() {
 		Collections.sort(noteBook, new Comparator<Record>() {
	        @Override
	        public int compare(Record r1, Record r2)
	        {
	            return  r1.phone.compareTo(r2.phone);
	        }
	    });
 	}
 	
 	/***
 	 *  базовая сортировка по умолчанию - по  убыванию фамилии
 	 */
 	public void sortByDefault() {
 		Collections.sort(noteBook);
 	}
 	
 	/***
 	 * лучше использовать toString
 	 */
 	public void noteBookShow() {
 		for (Record h :noteBook) {
 			h.show();
 		}
 	}

 	/***
 	 * переопределим метод строкового представления объекта.
 	 * удобно для вывода элементов на экран
 	 */
 	@Override
 	public String toString() {
 	  StringBuilder s = new StringBuilder(
 			 String.format("%18s%18s%18s%18s%18s%18s\n","Фамилия","Имя","Отчество","Телефон", "Адрес","Дата рождения")
 			 );
 	  
 	 for (Record h :noteBook) {
			s.append(h);
	}
 	  return s.toString();  
 	}
 	
 	
 	
}
