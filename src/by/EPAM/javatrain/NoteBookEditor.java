package by.EPAM.javatrain;

import java.util.Scanner;
import java.util.Collections;
import java.util.Date;

import java.util.Comparator;
import java.util.ArrayList;
import java.util.Arrays;

/***
 * Лекция 5. Задание 2
 * 
 * Создать класс “Записная книжка”. 
 * Предусмотреть возможность работы с произвольным числом записей, поиска записи по какому-либо признаку 
 * (например, по фамилии, дате рождения или номеру телефона), 
 * добавления и удаления записей, сортировки по разным полям.
 * @author Sergii_Kotov
 * 
 */

public class NoteBookEditor {
	static Scanner scanner = new Scanner(System.in);
	static NoteBook nb = new NoteBook();
	
	public static void main(String[] args) {
		// пример добавления записи с помощью ручного ввода
		nb.addRecord(new Record(enterS ("фамилию"),enterS ("имя"), enterS ("отчество"), 
								enterS ("телефон"), enterS ("адрес"),new Date()));
		//nb.noteBookShow();
		System.out.println(nb);
		System.out.println("Сортировка по умолчанию (по убыванию surname)");
		nb.sortByDefault();
		System.out.println(nb);
		System.out.println("Сортировка по имени (по возрастанию отдельным компаратором)");
		nb.sortByName();
		System.out.println(nb);
		System.out.println("Сортировка по телефону (по возрастанию отдельным компаратором)");
		nb.sortByPhone();
		System.out.println(nb);
		
		System.out.println("Добавление существующего");
		nb.addRecord(new Record("s0","имя", "отчество","0_132132", "адрес",new Date()));
		System.out.println("_______");
		System.out.println("Удаление существующего");
		nb.delRecord(new Record("s0","имя", "отчество","0_132132", "адрес",new Date()));
		System.out.println(nb);
	}
	
	/***
	 * запрос поля с клавиатуры
	 * @param имя требуемого поля
	 * @return значение поля введенное с клавиатуры
	 */
	static private String enterS (String fld){
		String s="";
		System.out.println("введите "+fld);
		if(scanner.hasNext()) {
			s=scanner.next();
		}
		return s;
	}
}
